package ru.t1.amsmirnov.taskmanager.dto.response.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;

public final class UserLockResponse extends AbstractResultResponse {

    public UserLockResponse() {
    }

    public UserLockResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}