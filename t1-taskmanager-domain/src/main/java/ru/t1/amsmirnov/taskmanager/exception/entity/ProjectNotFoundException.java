package ru.t1.amsmirnov.taskmanager.exception.entity;

public final class ProjectNotFoundException extends AbstractEntityNotFoundException {

    public ProjectNotFoundException() {
        super("Error! ProjectDTO not found...");
    }

}
