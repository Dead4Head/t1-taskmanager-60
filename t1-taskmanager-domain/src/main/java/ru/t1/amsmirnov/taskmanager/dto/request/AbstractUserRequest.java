package ru.t1.amsmirnov.taskmanager.dto.request;

import org.jetbrains.annotations.Nullable;

public abstract class AbstractUserRequest extends AbstractRequest {

    @Nullable
    protected String token;

    protected AbstractUserRequest() {

    }

    protected AbstractUserRequest(@Nullable final String token) {
        this.token = token;
    }

    @Nullable
    public String getToken() {
        return this.token;
    }

    public void setToken(@Nullable final String token) {
        this.token = token;
    }

}
