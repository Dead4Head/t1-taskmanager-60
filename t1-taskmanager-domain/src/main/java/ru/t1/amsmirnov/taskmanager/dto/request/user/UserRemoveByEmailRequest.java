package ru.t1.amsmirnov.taskmanager.dto.request.user;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class UserRemoveByEmailRequest extends AbstractUserRequest {

    @Nullable
    private String email;

    public UserRemoveByEmailRequest() {
    }

    public UserRemoveByEmailRequest(
            @Nullable final String token,
            @Nullable final String email
    ) {
        super(token);
        setEmail(email);
    }

    @Nullable
    public String getEmail() {
        return email;
    }

    public void setEmail(@Nullable final String login) {
        this.email = email;
    }

}