package ru.t1.amsmirnov.taskmanager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.api.provider.IConnectionProvider;
import ru.t1.amsmirnov.taskmanager.dto.request.user.*;
import ru.t1.amsmirnov.taskmanager.dto.response.user.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.net.MalformedURLException;

@WebService
public interface IUserEndpoint extends IEndpoint {

    @NotNull String NAME = "UserEndpoint";

    @NotNull String PART = NAME + "Service";

    @WebMethod(exclude = true)
    static IUserEndpoint newInstance() {
        return IEndpoint.newInstance(NAME, PART, IUserEndpoint.class);
    }

    @WebMethod(exclude = true)
    static IUserEndpoint newInstance(final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IUserEndpoint.class);
    }

    @NotNull
    @WebMethod
    UserChangePasswordResponse changePassword(@WebParam(name = REQUEST, partName = REQUEST) @NotNull UserChangePasswordRequest request);

    @NotNull
    @WebMethod
    UserLockResponse lockUserByLogin(@WebParam(name = REQUEST, partName = REQUEST) @NotNull UserLockRequest request);

    @NotNull
    @WebMethod
    UserRegistryResponse registry(@WebParam(name = REQUEST, partName = REQUEST) @NotNull UserRegistryRequest request);

    @NotNull
    @WebMethod
    UserRemoveResponse removeByLogin(@WebParam(name = REQUEST, partName = REQUEST) @NotNull UserRemoveRequest request);

    @NotNull
    @WebMethod
    UserRemoveByEmailResponse removeByEmail(@WebParam(name = REQUEST, partName = REQUEST) @NotNull UserRemoveByEmailRequest request);

    @NotNull
    @WebMethod
    UserUnlockResponse unlockUserByLogin(@WebParam(name = REQUEST, partName = REQUEST) @NotNull UserUnlockRequest request);

    @NotNull
    @WebMethod
    UserProfileResponse viewProfile(@WebParam(name = REQUEST, partName = REQUEST) @NotNull UserProfileRequest request);

    @NotNull
    @WebMethod
    UserUpdateResponse updateUserById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull UserUpdateRequest request);

}
