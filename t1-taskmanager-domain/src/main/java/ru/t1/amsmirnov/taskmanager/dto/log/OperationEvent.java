package ru.t1.amsmirnov.taskmanager.dto.log;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.enumerated.log.OperationType;

@JsonIgnoreProperties(ignoreUnknown = true)
public final class OperationEvent {

    @NotNull
    private OperationType type;

    @NotNull
    private Object entity;

    @Nullable
    private String tableName;

    @NotNull
    private Long timestamp = System.currentTimeMillis();

    public OperationEvent() {
    }

    public OperationEvent(
            @NotNull final Object entity,
            @NotNull final OperationType type
    ) {
        this.type = type;
        this.entity = entity;
    }

    @Nullable
    public String getTableName() {
        return tableName;
    }

    public void setTableName(@Nullable final String tableName) {
        this.tableName = tableName;
    }

    @NotNull
    public OperationType getType() {
        return type;
    }

    public void setType(@NotNull final OperationType type) {
        this.type = type;
    }

    @NotNull
    public Object getEntity() {
        return entity;
    }

    public void setEntity(@NotNull final Object entity) {
        this.entity = entity;
    }

    @NotNull
    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(@NotNull final Long timestamp) {
        this.timestamp = timestamp;
    }

}
