package ru.t1.amsmirnov.taskmanager.dto.response;

import org.jetbrains.annotations.NotNull;

public abstract class AbstractResultResponse extends AbstractResponse {

    private boolean success = true;

    @NotNull
    private String message = "";

    protected AbstractResultResponse() {
    }

    protected AbstractResultResponse(@NotNull final Throwable throwable) {
        setSuccess(false);
        setMessage(throwable.getMessage());
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
