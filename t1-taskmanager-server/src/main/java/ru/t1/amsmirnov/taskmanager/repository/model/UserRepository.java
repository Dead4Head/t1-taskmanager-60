package ru.t1.amsmirnov.taskmanager.repository.model;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.amsmirnov.taskmanager.api.repository.model.IUserRepository;
import ru.t1.amsmirnov.taskmanager.model.User;

import java.util.List;

@Repository
@Scope("prototype")
public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository() {
        super();
    }

    @NotNull
    @Override
    public List<User> findAll() {
        final String jql = "SELECT u FROM User u ORDER BY u.created";
        return entityManager.createQuery(jql, User.class).getResultList();
    }

    @NotNull
    @Override
    public List<User> findAllSorted(@Nullable final String sort) {
        final String jql = "SELECT u FROM User u ORDER BY :sort";
        return entityManager.createQuery(jql, User.class)
                .setParameter("sort", sort)
                .getResultList();
    }

    @Nullable
    @Override
    public User findOneById(@NotNull final String id) {
        final String jql = "SELECT u FROM User u WHERE u.id = :id";
        return entityManager.createQuery(jql, User.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findOneByLogin(@Nullable final String login) {
        final String jql = "SELECT u FROM User u WHERE u.login = :login";
        return entityManager.createQuery(jql, User.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findOneByEmail(@Nullable final String email) {
        final String jql = "SELECT u FROM UserDTO u WHERE u.email = :email";
        return entityManager.createQuery(jql, User.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        final String jql = "SELECT COUNT(u) FROM User u WHERE u.login = :login";
        final Long count = entityManager.createQuery(jql, Long.class)
                .setParameter("login", login)
                .getSingleResult();
        return count > 0;
    }

    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        final String jql = "SELECT COUNT(u) FROM User u WHERE u.email = :email";
        final Long count = entityManager.createQuery(jql, Long.class)
                .setParameter("email", email)
                .getSingleResult();
        return count > 0;
    }

    @Override
    public void removeAll() {
        final String jql = "DELETE FROM User";
        entityManager.createQuery(jql).executeUpdate();
    }

    @Override
    public void removeByLogin(@NotNull final String login) {
        entityManager.remove(findOneByLogin(login));
    }

}
