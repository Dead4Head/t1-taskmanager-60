package ru.t1.amsmirnov.taskmanager.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IAbstractRepository <M extends AbstractModel>{

    EntityManager getEntityManager();

    void add(@NotNull M model);

    void addAll(@NotNull Collection<M> models);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAllSorted(@Nullable String sort);

    @Nullable
    M findOneById(@NotNull String id);

    void update(@NotNull M model);

    void remove(@NotNull M model);

    void removeAll();

}
