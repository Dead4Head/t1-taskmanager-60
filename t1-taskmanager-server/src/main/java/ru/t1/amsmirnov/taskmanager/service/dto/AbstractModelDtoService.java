package ru.t1.amsmirnov.taskmanager.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.amsmirnov.taskmanager.api.repository.dto.IAbstractDtoRepository;
import ru.t1.amsmirnov.taskmanager.api.service.dto.IAbstractDtoService;
import ru.t1.amsmirnov.taskmanager.comparator.NameComparator;
import ru.t1.amsmirnov.taskmanager.comparator.StatusComparator;
import ru.t1.amsmirnov.taskmanager.dto.model.AbstractModelDTO;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ModelNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.IdEmptyException;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractModelDtoService<M extends AbstractModelDTO, R extends IAbstractDtoRepository<M>>
        implements IAbstractDtoService<M> {

    @NotNull
    protected R repository;

    public AbstractModelDtoService(@NotNull final R repository) {
        this.repository = repository;
    }

    @NotNull
    @Override
    @Transactional
    public M add(@Nullable final M model) throws AbstractException {
        if (model == null) throw new ModelNotFoundException();
        repository.add(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> addAll(@Nullable final Collection<M> models) throws AbstractException {
        if (models == null) throw new ModelNotFoundException();
        repository.addAll(models);
        return models;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> set(@Nullable final Collection<M> models) throws AbstractException {
        if (models == null) throw new ModelNotFoundException();
        removeAll();
        addAll(models);
        return models;
    }

    @NotNull
    @Override
    public List<M> findAll() throws AbstractException {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) throws AbstractException {
        return repository.findAllSorted(getComparator(comparator));
    }

    @NotNull
    @Override
    public M findOneById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final M model = repository.findOneById(id);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public M update(@Nullable final M model) throws AbstractException {
        if (model == null) throw new ModelNotFoundException();
        repository.update(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public M removeOne(@Nullable final M model) throws AbstractException {
        if (model == null) throw new ModelNotFoundException();
        repository.remove(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public M removeOneById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final M model = repository.findOneById(id);
        if (model == null) throw new ModelNotFoundException();
        repository.remove(model);
        return model;
    }

    @Override
    @Transactional
    public void removeAll() {
        repository.removeAll();
    }

    @Override
    public int getSize() throws AbstractException {
        return findAll().size();
    }

    @Override
    public boolean existById(@NotNull final String id) throws AbstractException {
        return repository.findOneById(id) != null;
    }

    protected String getComparator(Comparator<?> comparator) {
        if (comparator instanceof StatusComparator) return "status";
        if (comparator instanceof NameComparator) return "name";
        return "created";
    }

}
